﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._2
{
    internal class Schrikkeljaar
    {
        public static bool IsSchrikkelJaar(int jaar)
        {
            bool isEenSchrikkeljaar = false;

            if ((jaar % 4 == 0 && jaar % 100 != 0) || jaar % 400 == 0) 
            {
                isEenSchrikkeljaar = true;
            }

            return isEenSchrikkeljaar;
        }
    }
}
