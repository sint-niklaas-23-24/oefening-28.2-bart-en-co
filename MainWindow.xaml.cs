﻿using System.Windows;

namespace Oefening_28._2
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnSchrikkeljaar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lblSchrikkeljaar.Content = Schrikkeljaar.IsSchrikkelJaar(Convert.ToInt32(txtSchrikkeljaar.Text));
            }
            catch
            {
                MessageBox.Show("Er ging iets mis.");
            }

        }

        private void btnConversie_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lblConversieEur.Content = "€ " + EuroConversie.ToEuro(Convert.ToDecimal(txtConversie.Text));
                lblConversieBef.Content = "BEF " + EuroConversie.ToBef(Convert.ToDecimal(txtConversie.Text));
            }
            catch
            {
                MessageBox.Show("Er ging iets mis.");
            }

        }

        private void btnControle_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lblRrn.Content = Rijksregisternummer.isValid(txtControle.Password);
            }
            catch
            {
                MessageBox.Show("Er ging iets mis.");
            }

        }
    }
}