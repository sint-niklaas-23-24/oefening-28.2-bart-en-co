﻿namespace Oefening_28._2
{
    internal class Rijksregisternummer
    {
        public static bool isValid(string rrnr)
        {
            bool controle = false;

            if (rrnr.Length != 11)
            {
                controle = false;
            }

            string eersteNegenGetallen = rrnr.Substring(0, 9);
            string laatsteTweeGetallen = rrnr.Substring(9, 2);

            int eersteNegenGetallenWaarde;
            if (!int.TryParse(eersteNegenGetallen, out eersteNegenGetallenWaarde))
            {
                controle = false;
            }

            int restWaarde = eersteNegenGetallenWaarde % 97;
            int verwachteLaatsteTweeGetallen = 97 - restWaarde;

            int laatsteTweeGetallenWaarde;
            if (!int.TryParse(laatsteTweeGetallen, out laatsteTweeGetallenWaarde))
            {
                controle = false;
            }

            if (verwachteLaatsteTweeGetallen == laatsteTweeGetallenWaarde)
            {
                controle = true;
            }
            else controle = false;
            return controle;

        }

    }
}
